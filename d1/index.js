console.log("World");

// Object
/*
	-data type used to represent real world objects
	-collection of related data and/or functionalities
	-information stored in objects are represented in a "key:value" pair

	-"key" - "property" of an object.
	-"value" - actual data to be stored

	-different data type may be stored in a object's property creating complex data structure
*/

	// 2 ways of creating Object in JS
	/*
		1. object literal notation 
			// (let object = {}) **curly braces**
		2. object constructor notation
			//object instantation (let object = new object())
	*/

// Object Literal Notation
	// creating objects using initializer/literal notation
	// camelCase
	/*
		Syntax:

		let objectName = {
			keyA : valueA
			keyB : valueB
		}

	*/


	let cellphone = {
		name: "Nokia 3210",
		manufactureDate: 1999
	}

	console.log('Result from creating objects using literal notation');
	console.log(cellphone);

	let cellphone2 = {
		name : "Realme X2 pro",
		manufactureDate : 2020
	};
	console.log('Result from creating objects using literal notation');
	console.log(cellphone2);

	let ninja = {
		name : "Naruto Uzumaki",
		village : "Konoha",
		children : ["Boruto", "Himawari"]

	}
	console.log(ninja);

	// Object Constructor Notation
		// Creating object using a constructor function
		// creates a reusable "function" to create several objects that have the same data structure
		/*
			Syntax:

				function objectName(KeyA,KeyB){
	
				this.keyA = keyA:
				this.keyB = keyB;
				}
		*/

		// "this" keyword refers to the properties within the object
			// it allows the assignment of new object's properties
			// by associating them with values received from the constructor function's parameter


		function Laptop(name,manufactureDate){
			this.name = name;
			this.manufactureDate = manufactureDate;
		}

		let laptop = new Laptop('Lenovo', 2008);
		console.log('Result from creating object using object constructor');
		console.log(laptop);

		let myLaptop = new Laptop("Macbook Air", 2020);
		console.log('Result from creating object using object constructor');
		console.log(laptop);


		// "new" operator return statement
		let laptop1 = new Laptop('Asus', 2022);
		let laptop2 = new Laptop('Acer Predator', 2022);
		let laptop3 = new Laptop('HP', 2022);
		console.log(laptop1);
		console.log(laptop2);
		console.log(laptop3);

		let oldLaptop = Laptop("Portal R2E CCMC", 1980);
		console.log('Result from creating object using object constructor');
		console.log(oldLaptop);

		// Create empty objects

		let computer = {};
		let myComputer = new Object();
		console.log(computer);
		console.log(myComputer);

		// Accessing Object Properties

		// using dot notation

		console.log('Result of dot notation: ' + myLaptop.name);

		// using square bracket notation
			console.log('Result of square bracket notation: ' + myLaptop['name']);

		// accessing array of objects
		/*
			accessing object properties using the square bracket notation and array indexes can cause confusion
		*/

		let arrayObj = [laptop, myLaptop];
		// may be confused accessing array indexes
		console.log(arrayObj[0]['name']);
		// tells us that array[0] is an object by the dot notation
		console.log(arrayObj[0].name);

		// Initiallizing/adding/deleting/reassigning object properties
		// create an object using object literals

		let car = {};
		console.log('Current value of car object: ');
		console.log(car);

		// initializing/adding object properties
		car.name = "Honda Civic";
		console.log('Result from adding properties using dot notation: ');
		console.log(car);

		// initializing/adding object properties using notation (not recommended).

		console.log('Result of adding a property using square bracket notation: ');
		car['manufacture date'] = 2019;
		console.log(car);

		// Deleting Object Properties

		delete car['manufacture date'];
		console.log('Result from deleting properties: ');
		console.log(car);

		car['manufactureDate'] = 2019;
		console.log(car);

		// Reassigning Object Property values

		car.name = 'Toyota Vios';
		console.log('Result from reassigning property values: ');
		console.log(car);


		// Object Methods
		//a method is a function which is a property of an object

		let person = {
			name: 'John',
			talk: function(){
				console.log('Hello my name is ' + this.name);
			}

		}
		console.log(person);
		console.log('Result from object methods: ');
		person.talk();

		person.walk = function(steps){
			console.log(this.name + " walked " + steps + " steps forward");
		}
		console.log(person);
		person.walk(20)

		// Methods are useful for creating reusable functions that perform tasks related to objects

		let friend = {
			firstName: 'Joe',
			lastName: 'Smith',
			address: {
				city: 'Austin',
				country: 'Texas'
			},
			emails: ['joe@mail.com', 'joesmith@mail.xyz'],
			introduce: function(){
					console.log('Hello my name is ' + this.firstName + ' ' + this.lastName + ' ' + ' I Live in ' + this.address.city + ", " + this.address.country);
			}
		}
		friend.introduce();


		// Real World Application of Objects
		/*
			Scenarie:
			1. create a game that have several Pokemon interact with each other
			2. Every Pokemon would have the same set of stats, properties and function

			stats:
			name:
			level:
			health: level * 2
			attack: level

		*/
		// create an object constructor to lessen the process in creating the pokemon


	// function Pokemon(name,level){
	// 	// properties
	// 	this.name = name;
	// 	this.level = level;
	// 	this.health = level * 2;
	// 	this.attack = level;// 1.5
	// 	this.tackle = function(target){
	// 		console.log(this.name + " tackled " + target.name);
		
		
	// 	// target.health = target.health - this.attack
		
	// 	target.health -= this.attack

	// 	console.log(target.name + ' health is now reduce to ' + target.health);

	// 	if (target.health <= 0){
	// 		target.fainth()
	// 	}
		
	// 	}
	// 	this.faint = function(){
	// 			console.log(this.name + ' fainted')
	// 		}
	// }

	// let pikachu = new Pokemon ('Pikachu', 88);
	// console.log(pikachu);

	// let rattata = new Pokemon ('Rattata', 10);
	// console.log(rattata);





function Pokemon(name,level){

			//properties
			this.name = name;
			this.level = level;
			this.health = level * 2;
			this.attack = level;
			this.tackle = function(target){
				console.log(this.name + " tackled " + target.name);

				// target.health = target.health - this.attack
				target.health -= this.attack

				console.log(target.name + " health is now reduced to " + target.health);

				if(target.health<=0){
					target.faint()
				}

			}
			this.faint = function(){
				console.log(this.name + " fainted.")
			}

		}

		let pikachu = new Pokemon ("Pikachu", 88);
		console.log(pikachu);

		let rattata = new Pokemon("Ratata", 10);
		console.log(rattata);

