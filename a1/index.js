console.log("Hello");

let trainer = {
	name : 'Ash Ketchup',
	age :  10,
	pokemon : ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasur'],
	friends : {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	}

}
	console.log(trainer);
		trainer.talk = function(){
		console.log('Pikachu! I choose you!');
	}

	console.log('dot notation: ' + trainer.name);
	console.log('square bracket notation: ' + trainer['pokemon']);

trainer.talk()





		function Pokemon(name,level){
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level * 1.5;
	this.tackle = function(pokemonTarget){
			console.log(this.name + " tackled " + pokemonTarget.name);

			pokemonTarget.health -= this.attack

			console.log(pokemonTarget.name + " health is now reduced to " + pokemonTarget.health);

			if(pokemonTarget.health<=0){
					pokemonTarget.faint()				
				}
			}
	this.faint = function(){
				console.log(this.name + " fainted.")
			}

	
	
}

let pikachu = new Pokemon ('Pikachu', 10);
console.log(pikachu);
let geodude = new Pokemon ('Geodude', 60);
console.log(geodude);
let mewtwo = new Pokemon ('Mewtwo', 80);
console.log(mewtwo);
